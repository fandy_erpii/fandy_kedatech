# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError

class MaterialMaterial(models.Model):
    _name = 'material.material'


    name = fields.Char(string="Material Name")
    code = fields.Char(string="Material Code")
    material_type = fields.Selection([('fabric', 'Fabric'),('jeans', 'Jeans'),('cotton', 'Cotton')],string="Material Type")
    buy_price = fields.Float(string="Material Buy Price")
    supplier_id = fields.Many2one('res.partner', string="Name Supplier")


    @api.onchange('buy_price')
    def onchange_buy_price(self):
        if self.buy_price > 100:
            raise UserError('Price tidak boleh lebih dari 100 !!!')

