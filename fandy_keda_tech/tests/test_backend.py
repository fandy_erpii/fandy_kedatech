# -*- coding: utf-8 -*-
 
from odoo.tests import common
 
class TestBackend(common.TransactionCase):

    def setUp(self):
        # Create User
        test_partner = self.env['res.partner'].create({'name': 'client'})
        test_user = self.env['res.users'].create({'name': 'Client', 'login': 'client', 'email': 'client@example.com'})

        # Create material
        self.material1 = self.env['material.material'].create({
            'name': 'material A',
            'material_type': 'fabric',
        })
        self.material2 = self.env['material.material'].create({
            'name': 'material B',
            'material_type': 'jeans',
        })

        self.material3 = self.env['material.material'].create({
            'name': 'material C',
            'material_type': 'cotton',
        })


    def test_backend_1(self):
        """ See material and Filter
        """

        # See All material
        self.all_material = self.env['material.material'].sudo(test_user).search([])
        name_material = []
        for material in self.all_material:
            name_material.append([material.name])

        self.assertEqual(name_material, ['material A','material B','material C'])

        # Filter material
        self.filter_material = self.env['material.material'].sudo(test_user).search([('material_type','=','jeans')])
        name_material_filtered = []
        for material in self.filter_material:
            name_material_filtered.append([material.name])

        self.assertEqual(name_material_filtered, ['material B'])


    def test_backend_2(self):
        self.update_material = self.env['material.material'].sudo(test_user).search([('name','=','material A')])

        self.update_material.sudo(test_user).write({'name':'Material A update'})

        self.assertEqual(self.update_material.name, 'Material A update')


    def test_backend_3(self):
        self.delete_material = self.env['material.material'].sudo(test_user).search([('name','=','material C')])

        self.delete_material.sudo(test_user).unlink()

        search_material = self.env['material.material'].sudo(test_user).search([('name','=','material C')])

        self.assertEqual(search_material,False)


